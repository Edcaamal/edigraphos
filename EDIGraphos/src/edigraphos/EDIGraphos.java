/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edigraphos;

/**
 *
 * @author edgar
 */
public class EDIGraphos {

    public static Graph getCities() {
        Node camp = new Node("Campeche");
        Node tena = new Node("Tenabo");
 
        camp.addEdge(new Edge(camp, tena, 40));
 
        Graph graph = new Graph();
        graph.addNode(camp);
        graph.addNode(tena);

        return graph;
    }
    public static void main(String[] args) {
        Graph graph = getCities();
        System.out.println(graph);


    }
    
}
